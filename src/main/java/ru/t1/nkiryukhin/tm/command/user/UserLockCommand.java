package ru.t1.nkiryukhin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "user lock";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-lock";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
